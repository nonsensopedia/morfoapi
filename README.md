# morfoapi

REST API for accessing some features of the Morfeusz morphological analyzer. To be used with the Svetovid MediaWiki extension.

I am admittedly horrible at C++, and so is this program. Beware.

# Docker image
This wonder of technology comes with a preconfigured Docker image that *just works*™. The image uses the polimorf dictionary. You can run it using this command: `docker run -d registry.gitlab.com/nonsensopedia/morfoapi:latest`. The container will expose the service on the 8145 port, which you should probably connect to from a different container and not make it public on the host machine.

# INSTALLATION

I have no idea honestly.

You will definitely need the Morfeusz library: http://morfeusz.sgjp.pl/download/ Install so it can be found as a shared library.

And the Pistache library, also install the .so thing and you should be fine: http://pistache.io/

# Running this stuff

Just launch it and pray. You definitely should set the `-d` option to something meaningful (like `polimorf`, depending on the dictionary you have loaded).

Setting the `-p` (praeteritum) option to `composite` is advised. The thing will always bind to the loopback interface. Don't try exposing this API publicly, it's pretty much guaranteed to have tons of security holes, it's only suitable for internal use. The Svetovid extension has an API endpoint that passes on requests to this API.

It listens on the 8145 port by default, but that can be changed with the `--port` option.

morfoapi has a simple request/reply cache that stores words looked up recently, by default it's 128 words. You can change it using the `--cache-size` option.