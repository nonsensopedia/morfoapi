//
// Created by Ostrzyciel on 3/24/20.
//

#include "RequestCache.h"

RequestCache::RequestCache(const int size) {
    maxSize = size;
    cache = map<string, json>();
    evictionQueue = queue<string>();
}

std::optional<json> RequestCache::GetFromCache(const string& key) const {
    auto res = cache.find(key);
    if (res != cache.end()) return res->second;
    return std::optional<json>();
}

void RequestCache::StoreInCache(const string& key, const json& val) {
    // we assume the thing is not in cache already
    while (maxSize <= cache.size()) {
        auto toEvict = evictionQueue.front();
        evictionQueue.pop();
        cache.erase(toEvict);
    }

    evictionQueue.push(key);
    cache.emplace(key, val);
}
