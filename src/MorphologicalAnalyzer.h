//
// Created by Ostrzyciel on 10/7/19.
//

#ifndef MORFOAPI_MORPHOLOGICALANALYZER_H
#define MORFOAPI_MORPHOLOGICALANALYZER_H

#include <vector>
#include <tuple>
#include "morfeusz2.h"
#include "../cli/cli.hpp"
#include "LexemeForm.h"
#include "Lexeme.h"
#include "GrammaticalDescription.h"

using namespace morfeusz;
using namespace std;

class MorphologicalAnalyzer {
public:
    [[nodiscard]] static MorphologicalAnalyzer * getInstance();
    static MorphologicalAnalyzer * initializeInstance(ez::ezOptionParser& opt);

    [[nodiscard]] vector<tuple<string, GrammaticalDescription, int, bool>> analyzeWord(const string& word) const;
    [[nodiscard]] vector<LexemeForm> generateForms(const Lexeme& lexeme) const;
    [[nodiscard]] string getDictionaryId() const;

private:
    static MorphologicalAnalyzer* instance;
    Morfeusz* morfeusz;

    explicit MorphologicalAnalyzer(ez::ezOptionParser& opt);
    ~MorphologicalAnalyzer();
};


#endif //MORFOAPI_MORPHOLOGICALANALYZER_H