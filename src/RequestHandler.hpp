//
// Created by Ostrzyciel on 10/4/19.
//

#ifndef MORFOAPI_REQUESTHANDLER_HPP
#define MORFOAPI_REQUESTHANDLER_HPP

#include <pistache/endpoint.h>
#include "morfeusz2.h"
#include "RequestCache.h"

using namespace Pistache;
using namespace std;
using namespace morfeusz;

class RequestHandler : public Http::Handler {
private:
    RequestCache requestCache;

    void handleDeclension(const Http::Request& request, Http::ResponseWriter& response);
    static void handleInfo(const Http::Request& request, Http::ResponseWriter& response);

public:
    HTTP_PROTOTYPE(RequestHandler)

    explicit RequestHandler(int requestCacheSize);

    void onRequest(const Http::Request& request, Http::ResponseWriter response) override;
};

#endif //MORFOAPI_REQUESTHANDLER_HPP
