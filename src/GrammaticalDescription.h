//
// Created by Ostrzyciel on 4/24/20.
//

#ifndef MORFOAPI_GRAMMATICALDESCRIPTION_H
#define MORFOAPI_GRAMMATICALDESCRIPTION_H

#include <string>
#include <vector>

using namespace std;

class GrammaticalDescription {
public:
    explicit GrammaticalDescription(const string & tag);

    [[nodiscard]] string reconstructTag(bool withCase = true) const;
    void mergeCases(const vector<string> & cases);

    string pos, otherTags;
    vector<string> number, grCase, gender;

private:
    template <size_t Size>
    static int SplitSubtag(const string & tag, const string (&arr)[Size], vector<string> & result);
    static string glueTag(const vector<string> & tags);
};


#endif //MORFOAPI_GRAMMATICALDESCRIPTION_H
