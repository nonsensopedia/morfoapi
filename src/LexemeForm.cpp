//
// Created by Ostrzyciel on 10/27/19.
//

#include <algorithm>
#include "LexemeForm.h"
#include "constants.h"
#include "utils.hpp"
#include "GrammaticalDescription.h"

string LexemeForm::toString() {
    return grNumber + ";" + grCase + ";" + form + ";" + labels;
}

LexemeForm::LexemeForm(const string& form, const string& grNumber, const string& grCase) {
    this->form = form;
    this->grNumber = grNumber;
    this->grCase = grCase;
}

vector<LexemeForm> LexemeForm::CreateForms(const MorphInterpretation &interp, const Morfeusz *morfeusz) {
    auto orth = interp.orth;
    auto tag = interp.getTag(*morfeusz);
    auto labels = interp.getLabelsAsString(*morfeusz);
    return CreateForms(orth, tag, labels);
}

vector<LexemeForm> LexemeForm::CreateForms(const string& orth, const string &tag, const string &labels) {
    vector<LexemeForm> vec = vector<LexemeForm>();

    auto gdesc = GrammaticalDescription(tag);

    for (auto & number : gdesc.number) {
        for (auto & grCase : gdesc.grCase) {
            for (auto & gender : gdesc.gender) {
                LexemeForm lf = LexemeForm(orth, number, grCase);
                lf.pos = gdesc.pos;
                lf.gender = gender;
                lf.otherTags = gdesc.otherTags;
                lf.labels = labels;
                vec.push_back(lf);
            }
        }
    }

    return vec;
}

LexemeForm::LexemeForm() = default;

bool operator==(const LexemeForm &lf1, const LexemeForm &lf2) {
    return lf1.form == lf2.form &&
        lf1.pos == lf2.pos &&
        lf1.grNumber == lf2.grNumber &&
        lf1.gender == lf2.gender &&
        lf1.grCase == lf2.grCase &&
        lf1.labels == lf2.labels &&
        lf1.otherTags == lf2.otherTags;
}
