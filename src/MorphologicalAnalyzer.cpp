//
// Created by Ostrzyciel on 10/7/19.
//

#include <map>
#include <algorithm>
#include "MorphologicalAnalyzer.h"
#include "constants.h"
#include "utils.hpp"
#include "GrammaticalDescription.h"

MorphologicalAnalyzer* MorphologicalAnalyzer::instance;

MorphologicalAnalyzer::MorphologicalAnalyzer(ez::ezOptionParser &opt) {
    morfeusz = initializeMorfeusz(opt);
}

MorphologicalAnalyzer::~MorphologicalAnalyzer() {
    delete morfeusz;
}

MorphologicalAnalyzer * MorphologicalAnalyzer::getInstance() {
    return MorphologicalAnalyzer::instance;
}

MorphologicalAnalyzer * MorphologicalAnalyzer::initializeInstance(ez::ezOptionParser &opt) {
    MorphologicalAnalyzer::instance = new MorphologicalAnalyzer(opt);
    return MorphologicalAnalyzer::instance;
}

vector<tuple<string, GrammaticalDescription, int, bool>> MorphologicalAnalyzer::analyzeWord(const string& word) const {
    auto nDeclDict = map<string, tuple<string, GrammaticalDescription, int, bool>>();
    auto declDict = map<string, tuple<string, GrammaticalDescription, int, bool>>();
    auto vec = vector<tuple<string, GrammaticalDescription, int, bool>>();

    vector<MorphInterpretation> res;
    morfeusz->analyse(word, res);

    for (const auto& mi : res) {
        auto tag = mi.getTag(*morfeusz);
        auto gdesc = GrammaticalDescription(tag);
        int rating = 0;

        if (gdesc.pos == "depr") {
            rating--;
        }
        else if (gdesc.pos == "subst") {
            rating++;
        }

        bool declensible = false;
        for (int i = 0; i < extent<decltype(DeclensionValidTags)>::value; i++) {
            if (gdesc.pos == DeclensionValidTags[i]) {
                declensible = true;
                break;
            }
        }

        //maybe add more heuristics here
        if (!declensible) {
            nDeclDict.try_emplace(mi.lemma, make_tuple(mi.lemma, gdesc, rating, false));
            continue;
        }

        rating += 3;    //declensible, give higher priority
        auto key = mi.lemma + ":" + gdesc.reconstructTag(false);
        auto it = declDict.find(key);
        if (it == declDict.end()) {
            declDict.emplace(key, make_tuple(mi.lemma, gdesc, rating, true));
        } else {
            get<1>(it->second).mergeCases(gdesc.grCase);
        }
    }

    for (auto& a : nDeclDict) {
        vec.emplace_back(a.second);
    }

    for (auto& a : declDict) {
        vec.emplace_back(a.second);
    }

    sort(vec.begin(), vec.end(), [](auto & a, auto & b) { return get<2>(a) > get<2>(b); });

    return vec;
}

vector<LexemeForm> MorphologicalAnalyzer::generateForms(const Lexeme &lexeme) const {
    auto vec = vector<LexemeForm>();

    if ( !lexeme.declensible ) {
        for (const auto & GrNumber : GrNumbers)
            for (const auto & GrCase : GrCases)
                vec.emplace_back(lexeme.startForm.form, GrNumber, GrCase);

        return vec;
    }

    vector<MorphInterpretation> res;
    morfeusz->generate(lexeme.lemma, res);

    for (const auto & interp : res) {
        auto forms = LexemeForm::CreateForms(interp, morfeusz);

        if (lexeme.startForm.pos != forms[0].pos || lexeme.startForm.otherTags != forms[0].otherTags)
            continue;

        for (auto & lf : forms) {
            if (lexeme.startForm.gender == lf.gender && find(vec.begin(), vec.end(), lf) == vec.end())
                vec.push_back(lf);
        }
    }

    return vec;
}

string MorphologicalAnalyzer::getDictionaryId() const {
    return morfeusz->getDictID();
}
