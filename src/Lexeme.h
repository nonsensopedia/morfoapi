//
// Created by Ostrzyciel on 10/7/19.
//

#ifndef MORFOAPI_LEXEME_H
#define MORFOAPI_LEXEME_H

#include <string>
#include <vector>
#include <map>
#include "LexemeForm.h"

using namespace std;

class Lexeme {
public:
    static vector<Lexeme> createVectorFromStart(const string& startForm);

    Lexeme(const string& startForm, const string& lemma, const string& tag, bool declensible);

    [[nodiscard]] vector<LexemeForm> GenerateForms() const;

    [[nodiscard]] map<string, map<string, vector<string>>> GenerateFormsTree() const;

    string lemma, tag;
    LexemeForm startForm;
    bool declensible;
};


#endif //MORFOAPI_LEXEME_H
