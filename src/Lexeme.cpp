//
// Created by Ostrzyciel on 10/7/19.
//

#include "Lexeme.h"
#include "MorphologicalAnalyzer.h"

vector<Lexeme> Lexeme::createVectorFromStart(const string &startForm) {
    auto vec = vector<Lexeme>();
    auto analyzed = MorphologicalAnalyzer::getInstance()->analyzeWord(startForm);

    for (const auto& [ lemma, desc, _, decl ] : analyzed) {
        vec.emplace_back(startForm, lemma, desc.reconstructTag(), decl);
    }

    return vec;
}

Lexeme::Lexeme(const string &startForm, const string &lemma, const string &tag, const bool declensible) {
    this->startForm = LexemeForm::CreateForms(startForm, tag, "")[0];
    this->lemma = lemma;
    this->tag = tag;
    this->declensible = declensible;
}

vector<LexemeForm> Lexeme::GenerateForms() const {
    return MorphologicalAnalyzer::getInstance()->generateForms(*this);
}

map<string, map<string, vector<string>>> Lexeme::GenerateFormsTree() const {
    auto m = map<string, map<string, vector<string>>>();
    auto v = GenerateForms();

    for (const auto & lf : v) {
        auto num = lf.grNumber;
        if (num != "sg" && num != "pl")
            num = "sg";

        auto gCase = lf.grCase;
        if (gCase.empty())
            gCase = "nom";

        auto vecCase = m[num][gCase];
        vecCase.push_back(lf.form);
        m[num][gCase] = vecCase;    //why?
    }

    return m;
}

