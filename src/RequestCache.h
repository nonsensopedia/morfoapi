//
// Created by Ostrzyciel on 3/24/20.
//

#ifndef MORFOAPI_REQUESTCACHE_H
#define MORFOAPI_REQUESTCACHE_H

#include <queue>
#include <map>
#include <tuple>
#include <string>
#include "json.hpp"

using json = nlohmann::json;

using namespace std;

class RequestCache {
private:
    map<string, json> cache;
    queue<string> evictionQueue;
    int maxSize = 0;

public:
    explicit RequestCache(int size);
    RequestCache() = default;

    [[nodiscard]] std::optional<json> GetFromCache(const string& key) const;
    void StoreInCache(const string& key, const json& val);
};


#endif //MORFOAPI_REQUESTCACHE_H
