//
// Created by Ostrzyciel on 10/27/19.
//

#ifndef MORFOAPI_LEXEMEFORM_H
#define MORFOAPI_LEXEMEFORM_H

#include <string>
#include <vector>
#include "morfeusz2.h"

using namespace std;
using namespace morfeusz;

class LexemeForm {
public:
    LexemeForm();
    LexemeForm(const string& form, const string& grNumber, const string& grCase);

    static vector<LexemeForm> CreateForms(const MorphInterpretation & interp, const Morfeusz * morfeusz);
    static vector<LexemeForm> CreateForms(const string& orth, const string & tag, const string & labels);
    string toString();

    string form, grNumber, grCase, gender, pos, otherTags, labels;
};

bool operator==(const LexemeForm & lf1, const LexemeForm & lf2);

#endif //MORFOAPI_LEXEMEFORM_H
