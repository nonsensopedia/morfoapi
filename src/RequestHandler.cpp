//
// Created by Ostrzyciel on 10/5/19.
//

#include <iostream>
#include "RequestHandler.hpp"
#include "utils.hpp"
#include "Lexeme.h"
#include "morfeusz2.h"
#include "MorphologicalAnalyzer.h"
#include "json.hpp"

using json = nlohmann::json;

RequestHandler::RequestHandler(int requestCacheSize) {
    requestCache = RequestCache(requestCacheSize);
}

void RequestHandler::onRequest(const Http::Request& request, Http::ResponseWriter response) {
    if (request.method() != Http::Method::Get) {
        response.send(Http::Code::Method_Not_Allowed);
        return;
    }

    const string& resource = request.resource();

    if (resource == "/declension") {
        handleDeclension(request, response);
    } else if (resource == "/info") {
        handleInfo(request, response);
    } else {
        response.send(Http::Code::Not_Found);
    }
}

void RequestHandler::handleDeclension(const Http::Request &request, Http::ResponseWriter &response) {
    auto param = request.query().get("query").getOrElse("");

    if (param.empty() || param.size() > 2048) {
        response.send(Http::Code::Unprocessable_Entity);
        return;
    }

    param = url_decode(param);

    if (param.size() > 256) {
        response.send(Http::Code::Unprocessable_Entity);
        return;
    }

    stringstream ss(param);
    string token;
    json words;
    while (std::getline(ss, token, ' ')) {
        trim(token);
        if (token.empty()) continue;

        auto cached = requestCache.GetFromCache(token);
        if (cached.has_value()) {
            words.emplace_back(*cached);
            cerr << "\"" << token << "\" from cache" << endl;
            continue;
        }

        json interps;
        for (auto & l : Lexeme::createVectorFromStart(token)) {
            interps.emplace_back(json({
                {"lemma", l.lemma},
                // That's dumb, but it's easier to fix here than in PHP
                {"declensible", (string)(l.declensible ? "true" : "false")},
                {"tag", l.tag},
                {"forms", l.GenerateFormsTree()}
            }));
        }

        json res = {
            {"word", token},
            {"interpretations", interps}
        };
        requestCache.StoreInCache(token, res);
        words.emplace_back(res);
    }

    json j = {
        {"query", param},
        {"words", words}
    };
    cerr << param << endl;
    response.send(
        Pistache::Http::Code::Ok,
        j.dump(),
        MIME(Application, Json)
    );
}

void RequestHandler::handleInfo(const Http::Request &request, Http::ResponseWriter &response) {
    string responseText = "{";

    auto addKeyVal = [& responseText](const string& key, const string& val) {
        if (*(responseText.end() - 1) == '"') responseText += ",";
        responseText += "\"" + key + "\":\"" + val + "\"";
    };
    auto analyzer = MorphologicalAnalyzer::getInstance();

    addKeyVal("morfeuszVersion", morfeusz::Morfeusz::getVersion());
    addKeyVal("dictId", analyzer->getDictionaryId());

    responseText += "}";
    cerr << "info request";
    response.send(
            Pistache::Http::Code::Ok,
            responseText,
            MIME(Application, Json)
    );
}


