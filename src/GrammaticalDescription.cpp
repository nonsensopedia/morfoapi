//
// Created by Ostrzyciel on 4/24/20.
//

#include "GrammaticalDescription.h"
#include "constants.h"
#include "utils.hpp"
#include <numeric>
#include <algorithm>


GrammaticalDescription::GrammaticalDescription(const string &tag) {
    auto tagSplit = split_string(tag, ':');
    pos = tagSplit[0];     //part of speech, always!

    int it = 1;

    if (tagSplit.size() > it) {
        it += SplitSubtag(tagSplit[it], GrNumbers, number);
    } else {
        number.emplace_back("");
    }

    if (tagSplit.size() > it) {
        it += SplitSubtag(tagSplit[it], GrCases, grCase);
    } else {
        grCase.emplace_back("");
    }

    if (tagSplit.size() > it) {
        it += SplitSubtag(tagSplit[it], GrGenders, gender);
    } else {
        gender.emplace_back("");
    }

    for (; it < tagSplit.size(); it++) {
        otherTags += tagSplit[it];
        if (it != tagSplit.size() - 1)
            otherTags += ":";
    }
}

template<size_t Size>
int GrammaticalDescription::SplitSubtag(const string &tag, const string (&arr)[Size], vector<string> &result) {
    result = split_string(tag, '.');
    if (all_of(begin(arr), end(arr),
               [result](auto & s){ return find(result.begin(), result.end(), s) == result.end(); })) {
        result = vector<string>();
        result.emplace_back("");
        return 0;
    }

    return 1;
}

string GrammaticalDescription::reconstructTag(bool withCase) const {
    auto s = pos + ":" + glueTag(number);

    if (withCase) s += glueTag(grCase);
    s += glueTag(gender) + otherTags;
    if (s[s.length() - 1] == ':')
        return s.substr(0, s.length() - 1);

    return s;
}

string GrammaticalDescription::glueTag(const vector<string> &tags) {
    string s = accumulate(tags.cbegin(), tags.cend(), string(),
            [](const string& a, const string& b) { return a + "." + b; });
    s = s.substr(1);
    if (!s.empty()) return s + ":";
    return s;
}

void GrammaticalDescription::mergeCases(const vector<string> &cases) {
    for (const auto & a : cases)
        if (find(grCase.cbegin(), grCase.cend(), a) == grCase.cend())
            grCase.push_back(a);
}
