//
// Created by Ostrzyciel on 10/27/19.
//

#pragma once

#include <string>

using namespace std;

//przyimki mają przypadek jako cechę, ale są nieodmienne
const string DeclensionValidTags[] = {
        "subst", "depr", "ppron3", "ppron12", "siebie",
        "adj", "adjp", "ger", "pact", "ppas", "num"
};

const string GrNumbers[] = {
        "sg", "pl"
};

const string GrCases[] = {
        "nom", "gen", "dat", "acc", "inst", "loc", "voc"
};

const string GrGenders[] = {
        "m1", "m2", "m3", "n", "f"
};