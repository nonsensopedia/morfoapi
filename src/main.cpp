#include <cstdlib>
#include <iostream>
#include <vector>
#include "MorphologicalAnalyzer.h"
#include <pistache/endpoint.h>
#include "RequestHandler.hpp"

using namespace Pistache;
using namespace std;
using namespace morfeusz;

int main(int argc, const char** argv) {
    cerr << "Morfoapi, Morfeusz version: " << Morfeusz::getVersion() << endl;

    int port = 8145;
    int requestCacheSize = 128;
    auto addressName = "loopback";
    auto address = Pistache::Ipv4::loopback();

    try {
        ez::ezOptionParser& opt = *getOptions(argc, argv, GENERATOR);
        MorphologicalAnalyzer::initializeInstance(opt);

        if (opt.isSet("--public")) {
            addressName = "0.0.0.0 (ANY)";
            address = Pistache::Ipv4::any();
        }

        if (opt.isSet("--port")) {
            opt.get("--port")->getInt(port);
            cerr << "setting port to " << port << endl;
        }

        if (opt.isSet("--cache-size")) {
            opt.get("--cache-size")->getInt(requestCacheSize);
            cerr << "setting request cache size to " << requestCacheSize << endl;
        }

        delete &opt;
    }
    catch (const std::exception& ex) {
        cerr << ex.what() << endl;
        exit(1);
    }

    cerr << "Binding to " << addressName << ", port " << port << endl;

    Pistache::Address addr(address, Pistache::Port(port));
    auto opts = Pistache::Http::Endpoint::options()
            .threads(1);

    Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(Http::make_handler<RequestHandler>(requestCacheSize));
    cerr << "Server bound to interface, listening..." << endl;
    server.serve();

    return 0;
}