FROM ubuntu:focal AS builder

ENV DEBIAN_FRONTEND="noninteractive" TZ="UTC"

# Install build tools
WORKDIR /root
RUN apt-get update && apt-get install -y --no-install-recommends \
        wget \
        gpg-agent \
        cmake \
        software-properties-common \
        build-essential \
        meson \
        ninja-build \
        git

# Install morfeusz and polimorf
RUN wget -O - http://download.sgjp.pl/apt/sgjp.gpg.key | apt-key add - \
    && apt-add-repository http://download.sgjp.pl/apt/ubuntu \
    && apt-get update && apt-get install -y --no-install-recommends \
        libmorfeusz2-dev \
        morfeusz2 \
        morfeusz2-dictionary-polimorf \
    && git clone https://github.com/pistacheio/pistache.git

# Build the pistache library
WORKDIR /root/pistache
RUN git checkout ac763338f7126f19402cf2dfd64a0f7cfb4ec25c \
    && meson setup build \
        --buildtype=release \
        -DPISTACHE_USE_SSL=false \
        -DPISTACHE_BUILD_EXAMPLES=false \
        -DPISTACHE_BUILD_TESTS=false \
        -DPISTACHE_BUILD_DOCS=false \
        --prefix=$PWD/install \
    && ninja -C build \
    && meson install -C build \
    && cp -r install/include/* /usr/include \
    && cp -r install/lib/* /lib

# Build morfoapi
COPY . /root/morfoapi
WORKDIR /root/morfoapi
RUN cmake --configure . && cmake --build .

# Final container image
FROM ubuntu:focal
WORKDIR /root

# Copy binaries from the builder
COPY --from=builder /root/morfoapi/morfoapi /root/
COPY --from=builder /root/pistache/install/lib/* /lib/
COPY --from=builder /lib/libmorfeusz2.so /lib/
COPY --from=builder /usr/share/morfeusz2/dictionaries/* /root/

ENTRYPOINT ["./morfoapi"]
CMD ["-d", "polimorf", "-p", "composite", "--public"]
